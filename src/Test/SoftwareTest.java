package Test;

import gui.SoftwareFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import Test.SoftwareTest;
import Test.SoftwareTest.ListenerMgr;
import model.Cashcard;

public class SoftwareTest {
	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);

		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new SoftwareTest();
	}

	public SoftwareTest() {
		frame = new SoftwareFrame();
		frame.pack();
		frame.setVisible(true);
		frame.setSize(400, 400);
		list = new ListenerMgr();
		frame.setListener(list);
		setTestCase();
	}

	public void setTestCase() {
		/*
		 * Student may modify this code and write your result here
		 * frame.setResult("aaa\t bbb\t ccc\t"); frame.extendResult("ddd");
		 */
		Cashcard card = new Cashcard();
		
		frame.setResult(card.toString());
		frame.extendResult(card.deposit(3500));
		frame.extendResult(card.withdraw(1900));
		frame.extendResult(card.toString());
		
		
		//frame.setResult(stu1.toString());
		//frame.extendResult("\t" + "\t" + stu2.toString());

	}

	ActionListener list;
	SoftwareFrame frame;

}
